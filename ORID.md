## **O**：

-  At the standup meeting, I learned about some of the core programming ideas shared with us by the teacher: OO, Java, HTTP, etc. I understood that tools such as Spring are used to facilitate project development. During the process of learning these upper level tools, we should always keep in mind some of the core programming ideas and consider their relationship with the lower level.
-  In today's Code Review, I learned some knowledge about exception handling. The teacher mainly asked us to carefully study the pyramid model of Tests and understand the relationships, necessity, and granularity differences between various Tests.
-  In addition, I also learned about databases and used JPA tools to connect Spring Repository to the database. What is relatively unfamiliar is the use of association relationships, which supports defining association relationships between entity classes in Spring Data JPA. Define association relationships by using annotations such as @ JoinColumn, @ OneToMany, @ ManyToOne, etc. Relatively speaking, I don't know much about this part.
-  Tonight we will also do a PowerPoint presentation on CI/CD, and tomorrow we will share it in class.



## **R：**

- meaningful


## **I：**

- May still be unfamiliarity with some JPA syntax, which requires some time to understand when doing homework.


## **D：**

- Learn to search for information and solve problems when a problem gets stuck
