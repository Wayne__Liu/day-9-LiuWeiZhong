INSERT INTO companies (name) VALUES ('oocl');

INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Emily Wilson', 36, 'Female', 8200.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('David Thompson', 42, 'Male', 6900.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Jennifer Davis', 28, 'Female', 5900.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Matthew Wilson', 38, 'Male', 7200.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Jessica Johnson', 31, 'Female', 7800.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Daniel Martinez', 45, 'Male', 7100.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Sarah Anderson', 33, 'Female', 6800.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Christopher Clark', 39, 'Male', 6500.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Laura Lewis', 27, 'Female', 6000.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Ryan Rodriguez', 37, 'Male', 6700.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Megan Thomas', 32, 'Female', 7700.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Steven Martinez', 47, 'Male', 7000.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Emily Baker', 34, 'Female', 6600.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Andrew Adams', 29, 'Male', 5900.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Olivia White', 41, 'Female', 7900.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Justin Turner', 35, 'Male', 7100.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Amanda Wilson', 30, 'Female', 6200.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Brandon Garcia', 44, 'Male', 6700.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Elizabeth Taylor', 26, 'Female', 6100.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Nicholas Hernandez', 36, 'Male', 7300.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Sophia Jackson', 31, 'Female', 7700.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Jacob Clark', 43, 'Male', 7600.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Madison Lewis', 27, 'Female', 6000.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Michael Wilson', 38, 'Male', 7200.0, 1);

